The OpenCraft Mattermost chat bot
---------------------------------

To run the bot, create a virtualenv, install the requirements using

    pip install -r requirements.txt

and start the bot using

    MATTERMOST_BOT_PASSWORD=<password> MATTERMOST_BOT_SETTINGS_MODULE=settings matterbot
