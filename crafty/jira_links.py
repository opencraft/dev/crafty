"""Reply to messages containing Jira issue numbers with the full issue links."""

import collections
import re

from mattermost_bot.bot import listen_to, respond_to

# Configuration

OPENCRAFT_JIRA = "tasks.opencraft.com"
EDX_JIRA = "openedx.atlassian.net"
MCKINSEY_JIRA = "edx-wiki.atlassian.net"
JIRA_PROJECTS = {
    "ADMIN": OPENCRAFT_JIRA,
    "BIZ": OPENCRAFT_JIRA,
    "HMS": OPENCRAFT_JIRA,
    "OC": OPENCRAFT_JIRA,
    "PEAR": OPENCRAFT_JIRA,
    "MCKIN": MCKINSEY_JIRA,
}
DEFAULT_PROJECT = EDX_JIRA

ISSUE_REGEX = r"[A-Z]{2,7}-\d{1,6}"
URL_REGEX = r"https?://\S+/{}".format(ISSUE_REGEX)


class RecentFilter:
    """Filter out links that were recently posted."""

    def __init__(self, maxlen=6):
        self._history = collections.deque(maxlen=maxlen)

    def add(self, items):
        self._history.append(items)

    def filter(self, new_items):
        all_items = set().union(*self._history)
        new_items = [item for item in new_items if item not in all_items]
        if new_items:
            self.add(new_items)
        return new_items


# Global recent filters variable.  To be able to run more than one worker, we
# need to make this thread-safe.  Best solution: Move the data to Redis.  This
# would also persist the data across restarts.
recent_links = collections.defaultdict(RecentFilter)


def get_issue_link(issue):
    """Get the full issue link for the given Jira issue."""
    project = issue.split("-")[0]
    domain = JIRA_PROJECTS.get(project, DEFAULT_PROJECT)
    return "https://{domain}/browse/{issue}".format(domain=domain, issue=issue)


@listen_to("")
@respond_to("")
def jira_links(message):
    """Parse message for Jira issue numbers and reply with full links."""
    channel = message.get_channel_name()
    text = message.get_message()
    text_links = re.findall(URL_REGEX, text)
    recent_links[channel].add(text_links)
    issues = re.findall(ISSUE_REGEX, text)
    issue_links = recent_links[channel].filter(map(get_issue_link, issues))
    if issue_links:
        formatted_links = "\n".join("* {}".format(link) for link in issue_links)
        message.send(formatted_links)
